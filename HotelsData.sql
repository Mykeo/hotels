USE Hotels;

INSERT INTO Clients (firstName, lastName, country) VALUE 
	("Michael", "Longroad", "US"), 
    ("Taylor", "Nightsky", "US"), 
    ("John", "Plainmen", "US"), 
    ("Boris", "Tolchanov", "UK"), 
    ("John", "Merking", "UK"),
    ("John", "Cleverwoods", "AU"), 
    ("Lola", "Softlands", "UK");

INSERT INTO Surveys (clientId, reviewDate, review, stars) VALUE 
	(1, "2020-05-11", "It was fantastic!", 5),
    (1, "2020-05-18", "Really amazing experience!", 5), 
    (1, "2020-07-26", "Yeah, it is relaxing.", 4), 
    (2, "2020-05-19", "Could be better, actually.", 3), 
    (4, "2020-04-02", "I'm completely fine with it.", NULL), 
    (4, "2020-04-03", "I will try it.", NULL),
    (5, "2020-05-28", NULL, 5);

INSERT INTO Layouts (layoutDesc, price) VALUE
	("Еwo-bedroom apartment in new luxurious building.", 499),
    ("A three-room flat with wooden ceilings and bricks.", 249), 
    ("Cozy studio apartment that overlooks  the seashore.", 379), 
    ("Excellent one-bedroom serviced apartment", 349), 
    ("Spacy apartments with luxurious furniture.", 1099);

INSERT INTO Hotels (hotelName, adress, floors, stars, price) VALUE 
	("Primo Albergo", "IT", 4, 5, 299),
    ("Secondo Hotel", "IT", 6, 4, 219),
    ("Tercer Hotel", "ES", 4, 4, 199);

INSERT INTO Numbers (layoutId, hotelId, floorNumber, apartamentNumber, vacant) VALUE 
	(1, 1, 1, 1, TRUE),
    (2, 3, 1, 1, TRUE),
    (3, 1, 2, 2, TRUE),
	(4, 2, 3, 1, TRUE),
    (5, 2, 4, 2, FALSE);

INSERT INTO Invoices (clientId, hotelId, numberId, startDate, endDate) VALUE 
	(1, 1, 1, "2020-05-11", "2020-05-19"),
    (2, 1, 1, "2020-05-11", "2020-05-19"),
    (3, 3, 2, "2020-06-03", "2020-06-18"),
    (4, 1, 3, "2020-03-25", "2020-04-05"),
    (5, 2, 4, "2020-05-21", "2020-05-29"),
    (1, 2, 5, "2020-07-19", "2020-08-01");

INSERT INTO Services (serviceName, serviceDesc, price, available) VALUE 
	("Morning meal.", "A cup of tea with a croissant.", 6.99, TRUE),
    ("Oolong Tea", "A cup of expensive tea.", 2.99, TRUE),
    ("Croissant with blackberries", "Really tasty bun.",  4.99, TRUE),
    ("Dessert for two.", "Milkshakes for two as well as Sandy.", 11.59, TRUE),
    ("Sandy", "A cheesecake.", 6.49, TRUE),
    ("Milkshake", "A sweet drink made by blending milk, ice cream, and flavorings.", 3.19, TRUE);

INSERT INTO Offers (serviceId, parentServiceId) VALUE 
	(2, 1), (3, 1), (5, 4), (6, 4), (6, 4);

INSERT INTO Receipts (clientId, serviceId, receiptTime) VALUE 
	(1, 1, "2020-05-11 7:56:03"),
    (1, 2, "2020-05-19 8:20:46"),
    (1, 1, "2020-05-19 7:21:13"),
    (3, 2, "2020-06-08 14:06:15"),
    (3, 2, "2020-06-14 15:01:33"),
    (5, 5, "2020-07-19 11:12:10");
