CREATE DATABASE IF NOT EXISTS Hotels;

USE Hotels;

CREATE TABLE IF NOT EXISTS Clients (
    clientId INT AUTO_INCREMENT PRIMARY KEY,
    firstName VARCHAR(64) NOT NULL,
    lastName VARCHAR(64) NOT NULL,
    country VARCHAR(2) NOT NULL
);

CREATE TABLE IF NOT EXISTS Surveys (
    surveyId INT AUTO_INCREMENT PRIMARY KEY,
    clientId INT NOT NULL,
    reviewDate DATE NOT NULL,
    review VARCHAR(512),
    stars INT,
    FOREIGN KEY (clientId)
        REFERENCES Clients (clientId),
    INDEX (reviewDate)
);

CREATE TABLE IF NOT EXISTS Layouts (
    layoutId INT AUTO_INCREMENT PRIMARY KEY,
    layoutDesc VARCHAR(512) NOT NULL,
    price DECIMAL(6,2) NOT NULL
);

CREATE TABLE IF NOT EXISTS Hotels (
	hotelId INT AUTO_INCREMENT PRIMARY KEY,
    hotelName VARCHAR(64) NOT NULL,
    adress VARCHAR(256) NOT NULL,
    floors INT NOT NULL,
    stars INT NOT NULL,
    price DECIMAL(6,2) NOT NULL
);

CREATE TABLE IF NOT EXISTS Numbers (
    numberId INT AUTO_INCREMENT PRIMARY KEY,
    layoutId INT UNIQUE NOT NULL,
    hotelId INT NOT NULL,
    floorNumber INT NOT NULL,
    apartamentNumber INT NOT NULL,
    vacant BOOLEAN NOT NULL,
    FOREIGN KEY (layoutId)
        REFERENCES Layouts (layoutId),
    FOREIGN KEY (hotelId)
        REFERENCES Hotels (hotelId)
);

CREATE TABLE IF NOT EXISTS Invoices (
    invoiceId INT AUTO_INCREMENT PRIMARY KEY,
    clientId INT NOT NULL,
    hotelId INT NOT NULL,
    numberId INT NOT NULL,
    startDate DATE NOT NULL,
    endDate DATE NOT NULL,
    FOREIGN KEY (clientId)
        REFERENCES Clients (clientId),
    FOREIGN KEY (hotelId)
        REFERENCES Hotels (hotelId),
    FOREIGN KEY (numberId)
        REFERENCES Numbers (numberId),
    INDEX (startDate , endDate)
);

CREATE TABLE IF NOT EXISTS Services (
    serviceId INT AUTO_INCREMENT PRIMARY KEY,
    serviceName VARCHAR(128) NOT NULL,
    serviceDesc VARCHAR(256) NOT NULL,
    available BOOLEAN NOT NULL,
    price DECIMAL(6,2) NOT NULL
);

CREATE TABLE IF NOT EXISTS Offers (
	offerId INT AUTO_INCREMENT PRIMARY KEY,
    serviceId INT NOT NULL,
    parentServiceId INT NOT NULL,
    FOREIGN KEY (serviceId)
        REFERENCES Services (serviceId),
	FOREIGN KEY (parentServiceId)
        REFERENCES Services (serviceId)
);

CREATE TABLE IF NOT EXISTS Receipts (
    receiptId INT AUTO_INCREMENT PRIMARY KEY,
    clientId INT NOT NULL,
    serviceId INT NOT NULL,
    receiptTime DATETIME NOT NULL,
    FOREIGN KEY (clientId)
        REFERENCES Clients (clientId),
    FOREIGN KEY (serviceId)
        REFERENCES Services (serviceId),
    INDEX (receiptTime)
);
