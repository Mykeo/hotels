USE Hotels;

CREATE OR REPLACE VIEW Opinions AS
    SELECT 
        *
    FROM
        Clients
            JOIN
        Surveys USING (clientId);

CREATE OR REPLACE VIEW Apartaments AS
    SELECT 
        *
    FROM
        Numbers
            JOIN
        Layouts USING (layoutId);

CREATE OR REPLACE VIEW Expenses AS
    SELECT 
        *
    FROM
        Receipts
            JOIN
        Services USING (serviceId)
            JOIN
        Clients USING (clientId);
        
CREATE OR REPLACE VIEW Spendings AS
    SELECT 
        invoiceId,
        Invoices.clientId,
        startDate,
        receiptTime,
        endDate,
        receiptId,
        serviceId,
        serviceName,
        serviceDesc,
        price
    FROM
        Invoices
            LEFT JOIN
        Expenses ON (Invoices.clientId = Expenses.clientId)
            AND (Expenses.receiptTime BETWEEN Invoices.startDate AND Invoices.endDate);

CREATE OR REPLACE VIEW ResidenceCosts (hotelId , numberId , hotelPrice , numberPrice , residencePrice) AS
    SELECT 
        hotelId,
        numberId,
        Hotels.price,
        Apartaments.price,
        Hotels.price + Apartaments.price
    FROM
        Apartaments
            JOIN
        Hotels USING (hotelId);

CREATE OR REPLACE VIEW Pricing (invoiceId , hotelPrice , numberPrice , residencePrice , expenses , cost) AS
    SELECT 
        invoiceId,
        hotelPrice,
        numberPrice,
        residencePrice,
        COALESCE(SUM(price), 0),
        residencePrice + COALESCE(SUM(price), 0)
    FROM
        Invoices
            LEFT JOIN
        ResidenceCosts USING (hotelId , numberId)
            LEFT JOIN
        Spendings USING (invoiceId)
    GROUP BY invoiceId;

CREATE OR REPLACE VIEW TripReviews AS
    SELECT 
        Clients.clientId,
        Invoices.invoiceId,
        Surveys.surveyId,
        startDate,
        reviewDate,
        endDate,
        review,
        stars
    FROM
        Clients
            INNER JOIN
        Surveys ON (Clients.clientId = Surveys.clientId)
            INNER JOIN
        Invoices ON (Clients.clientId = Invoices.clientId)
            AND (reviewDate BETWEEN Invoices.startDate AND Invoices.endDate);
 
CREATE OR REPLACE VIEW Destinations AS
    SELECT 
        Clients.clientId AS 'clientId',
        Invoices.invoiceId AS 'invoiceId',
        Hotels.hotelId AS 'hotelId',
        Numbers.numberId AS 'numberId',
        firstName,
        lastName,
        hotelName,
        adress,
        floors,
        floorNumber,
        apartamentNumber
    FROM
        Clients
            INNER JOIN
        Invoices USING (clientId)
            INNER JOIN
        Hotels USING (hotelId)
            INNER JOIN
        Numbers USING (numberId);

CREATE OR REPLACE VIEW Proposals AS
    SELECT 
        parents.serviceId AS 'serviceId',
        parents.serviceName AS 'serviceName',
        parents.serviceDesc AS 'serviceDesc',
        children.serviceId AS 'offerId',
        children.serviceName AS 'offerName',
        children.serviceDesc AS 'offerDesc',
        parents.price AS 'servicePrice',
        children.price AS 'offerPrice'
    FROM
        Offers
            LEFT JOIN
        Services parents USING (serviceId)
            LEFT JOIN
        Services children ON (Offers.parentServiceId = children.serviceId);

CREATE OR REPLACE VIEW Discounts AS
    SELECT 
        offerId,
        offerName,
        offerDesc,
        SUM(servicePrice) AS 'individualCost',
        offerPrice,
        ROUND(1 - offerPrice / SUM(servicePrice), 2) AS 'offerDiscount'
    FROM
        Proposals
    GROUP BY offerId;
 
CREATE OR REPLACE VIEW VacantRooms (hotelId , hotelName , hotelAdress , floorNumber , vacantNumbers) AS
    SELECT 
        hotelId, hotelName, adress, floorNumber, COUNT(*)
    FROM
        Hotels
            INNER JOIN
        Numbers USING (hotelId)
    WHERE
        vacant IS TRUE
    GROUP BY hotelId , floorNumber
    ORDER BY COUNT(*) DESC , hotelName , adress;