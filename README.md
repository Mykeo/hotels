## Hotels

Small database designed for a small hotel service.

### Design

The database consists of following relations shown in the ER diagram.

![ER](./Hotels.png)

These relations are used to run the views:

* Opinions, contains clients and surveys they have taken.
* Apartaments, contains numbers and their layouts.
* Expenses, contains clients and cervices they have accepted.
* Spendings, contains invoices and services accepted during the time.
* ResidenceCosts, contains how much money it takes to rent an apartment.
* Pricing, contains invoice's price a client has to cover.
* TripReviews, contains invoices and surveys taken during the time.
* Destinations, contains where a client lives.
* Proposals, contains offers and services the offer is based on.
* Discounts, contains how much cheaper it would be to accept the offer than to pay for everything individually.
* VacantRooms, contains vacant rooms.